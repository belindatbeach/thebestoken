#ifndef QTIPCSERVER_H
#define QTIPCSERVER_H

// Define tbttoken-Qt message queue name
#define BITCOINURI_QUEUE_NAME "tbttokenURI"

void ipcScanRelay(int argc, char *argv[]);
void ipcInit(int argc, char *argv[]);

#endif // QTIPCSERVER_H
